//Función que realizara la petición al servicio web
function peticion(){
    
    //Declaración de variables necesarias
    const http = new XMLHttpRequest;    //Creación del objeto XMLHttpRequest
    const url = "https://jsonplaceholder.typicode.com/photos"

    http.onreadystatechange= function(){

        //Validación de la respuesta de la petición
        if((http.status == 200) && (http.readyState == 4)){
            console.log(http.responseText); //Devuelve el texto recibido del servidor.

            let rep = document.getElementById("reply");
            rep.innerHTML = "";     //Se limpia html

            const frag = document.createDocumentFragment();//Fragment es una versión ligera del Document.
            
            const json = JSON.parse(this.responseText);

            for(const datos of json){
                const li = document.createElement("tr"); //Al tener el elemento "tr" no es necesarión incluirlo en el innerHTML
                    li.innerHTML = `<td>${datos.albumId}</td>
                    <td>${datos.id}</td>
                    <td>${datos.title}</td>
                    <td><img src="${datos.thumbnailUrl}" class="thumbnail w-100 h-100" onclick="alerta('${datos.url}')"></td>`

                frag.appendChild(li);

            }// Termina el ciclo for
            rep.appendChild(frag);

        }// Termina el if

    }// Termina la función de validación.

    http.open('GET', url, true); //Inicia la solicitud.
    http.send();    //Envía la solicitud al servidor

}// Termina la función de petición

//Función para limpiar la pantalla.
function limpiar(){
    let rep = document.getElementById("reply");
    rep.innerHTML = ""; 
}

function alerta(url) {
    const modalToggle = document.getElementById("alertModal");
    const myModal = new bootstrap.Modal("#alertModal", { keyboard: false });

    document.getElementById("alertImg").src = url;

    myModal.show(modalToggle);
}


//Botones
document.getElementById("btnPeticion").addEventListener("click", peticion);
document.getElementById("btnLimpiar").addEventListener("click", limpiar);